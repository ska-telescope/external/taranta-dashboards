'use strict';
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
should;

var mongoose = require('mongoose');

// Import server
var server = require('../app');

// Import Todo Model
var UserActionLog = require('../models/userActionLog');

const jsonwebtoken = require('jsonwebtoken');

jest.mock('jsonwebtoken');

jsonwebtoken.verify.mockReturnValue(
  {
    username: 'MyUser',
    groups: 'group1',
  },
);

// Clear database after all tests are done
afterAll(async() => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

// use chaiHttp for making the actual HTTP requests
chai.use(chaiHttp);

describe('userActionLog API', function() {

  it('Adds example ExcuteCommandUserAction action', function(done) {
    var MyUserAction = new UserActionLog({
      actionType: 'ExcuteCommandUserAction',
      user: 'MyUser',
      tangoDB: 'tangotest',
      device: 'testdevice1',
      name: 'On',
      value: '1',
      argin: '1',
      valueBefore: '0',
      valueAfter: '1',
    });
    chai.request(server)
      .post('/logs/saveUserAction')
      .send(MyUserAction)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('Adds example SetAttributeValueUserAction action', function(done) {
    var MyUserAction = new UserActionLog({
      actionType: 'SetAttributeValueUserAction',
      user: 'MyUser',
      tangoDB: 'tangotest',
      device: 'testdevice1',
      name: 'On',
      value: '1',
      argin: '1',
      valueBefore: '0',
      valueAfter: '1',
    });
    chai.request(server)
      .post('/logs/saveUserAction')
      .send(MyUserAction)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('Adds example PutDevicePropertyUserAction action', function(done) {
    var MyUserAction = new UserActionLog({
      actionType: 'PutDevicePropertyUserAction',
      user: 'MyUser',
      tangoDB: 'tangotest',
      device: 'testdevice1',
      name: 'On',
      value: '1',
      argin: '1',
      valueBefore: '0',
      valueAfter: '1',
    });
    chai.request(server)
      .post('/logs/saveUserAction')
      .send(MyUserAction)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('Adds example DeleteDevicePropertyUserAction action', function(done) {
    var MyUserAction = new UserActionLog({
      actionType: 'DeleteDevicePropertyUserAction',
      user: 'MyUser',
      tangoDB: 'tangotest',
      device: 'testdevice1',
      name: 'On',
      value: '1',
      argin: '1',
      valueBefore: '0',
      valueAfter: '1',
    });
    chai.request(server)
      .post('/logs/saveUserAction')
      .send(MyUserAction)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('Adds example default action', function(done) {
    var MyUserAction = new UserActionLog({
      actionType: 'command',
      user: 'MyUser',
      tangoDB: 'tangotest',
      device: 'testdevice1',
      name: 'On',
      value: '1',
      argin: '1',
      valueBefore: '0',
      valueAfter: '1',
    });
    chai.request(server)
      .post('/logs/saveUserAction')
      .send(MyUserAction)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('reads userActionLogs', function(done) {

    chai.request(server)
      .get('/logs/userActionLogs?tangoDB=tangotest&device=testdevice1&limit=5')
      .end(function(err, res) {
        if (err) console.log(err);
        res.body.device.userActions.should.be.a('array');
        res.body.device.userActions.should.to.not.be.empty;
        res.status.should.be.equal(200);
        done();
      });
  });

});
