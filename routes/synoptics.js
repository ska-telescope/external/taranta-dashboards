'use strict';
/**
 * Handles urls prefixed with /synoptics
 */
const express = require('express');
const jsonwebtoken = require('jsonwebtoken');
const mongoose = require('mongoose');
const router = express.Router();
var Synoptic = require('../models/synoptic');

/**
 * Returns a count of shared synoptics
 * for each group the current user belongs to, for a specific tangodb.
 */
router.get('/group/synopticsCount', async(req, res, next) => {
  const token = req.cookies.taranta_jwt;
  const excludeCurrentUser = req.query.excludeCurrentUser === 'true';
  const tangoDB = req.query.tangoDB || '';
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error){
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const data = {};
  for (let i = 0; i < groups.length; i++){
    try {
      // allowing missing tango db is really just for backwards compatibility.
      // Can be removed when all synoptic has this set
      const query = {
        group: groups[i],
        deleted: { $ne: true },
        tangoDB: {$in: ['', tangoDB, null]},
      };
      if (excludeCurrentUser){
        query['user'] = {$ne: username};
      }
      data[groups[i]] = await Synoptic.countDocuments(query);
    } catch (error){
      console.log(error);
    }
  }
  res.send(data);
});
/**
 * Return a list of synoptic that belongs to a specific group
 * for a specific tangodb.
 * Does not return the SVG
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * group: AD group this synoptic is shared with, or null
 */
router.get('/group/synoptics', function(req, res, next) {
  const token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const requestedGroup = req.query.group;
  const tangoDB = req.query.tangoDB || '';
  const excludeCurrentUser = req.query.excludeCurrentUser === 'true';
  if (!requestedGroup){
    res.status(400).send('Missing query parameter group');
    return;
  }
  if (!groups.includes(requestedGroup)) {
    res.sendStatus(403);
    return;
  }
  const query = {
    group: requestedGroup,
    deleted: { $ne: true },
    tangoDB: {$in: ['', tangoDB, null]},
  };
  if (excludeCurrentUser){
    query['user'] = {$ne: username};
  }
  Synoptic.find(query, function(
    err,
    result,
  ) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        var list = [];
        result.forEach(function(result) {
          console.log(result);
          const {
            name,
            user,
            insertTime,
            updateTime,
            group,
            groupWriteAccess,
            lastUpdatedBy,
            tangoDB,
            variables,
          } = result;
          list.push({
            id: result._id,
            name,
            user,
            insertTime,
            updateTime,
            group,
            groupWriteAccess,
            lastUpdatedBy,
            tangoDB,
            variables,
          });
        });
        res.send(list);
      }
    }
  });
});

/**
 * Return a list of synoptic that belongs to a specific user
 * for a specific tangodb.
 * Does not return the SVG
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * group: AD group this synoptic is shared with, or null
 */
router.get('/user/synoptics', function(req, res, next) {
  const token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    console.log('decodeToken error: ', error);
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log(
    'Get called for username : ',
    username,
    ' group: ',
    groups,
  );

  const tangoDB = req.query.tangoDB || '';
  console.log('tangoDB: ', tangoDB);

  Synoptic.find(
    {
      user: username,
      deleted: { $ne: true },
      tangoDB: { $in: ['', tangoDB, null] },
    },
    function(err, result) {
      if (err) {
        res.sendStatus(404);
      } else {
        if (result) {
          var list = [];
          result.forEach(function(result) {
            const {
              name,
              user,
              insertTime,
              updateTime,
              group,
              lastUpdatedBy,
              tangoDB,
              variables,
            } = result;
            list.push({
              id: result._id,
              name,
              user,
              insertTime,
              updateTime,
              group,
              lastUpdatedBy,
              tangoDB,
              variables,
            });
          });
          res.send(list);
        }
      }
    },
  );
});

/**
 * Return a full synoptic by id.
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * svg: string
 */
router.get('/:id', function(req, res, next) {
  var id = req.params.id;
  Synoptic.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        console.log('Serving synoptic: ' + result._id);
        const {
          name,
          user,
          variables,
          insertTime,
          updateTime,
          group,
          groupWriteAccess,
          lastUpdatedBy,
          tangoDB,
        } = result;
        res.send({
          id: result._id,
          name,
          user,
          insertTime,
          updateTime,
          svg: result.svg,
          variables,
          group,
          groupWriteAccess,
          lastUpdatedBy,
          tangoDB,
        });
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Delete a synoptic that belongs to the user by id
 * returns
 * id: string
 * deleted: boolean
 */
router.delete('/:id', function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log(
    'Delete called for username : ',
    username,
    ' group: ',
    groups,
    ' id : ',
    id,
  );

  Synoptic.findOneAndUpdate(
    { _id: id, user: username, deleted: { $ne: true } },
    { $set: { deleted: true } },
    { new: true },
    function(err, result) {
      if (err) {
        res.send({ id, deleted: false });
      } else {
        if (result) {
          res.send({ id: result._id, deleted: true });
        } else {
          res.sendStatus(403);
        }
      }
    },
  );
});
/**
 * Share a synoptic to an AD group
 */
router.post('/:id/share', function(req, res) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  var group = req.body.group;
  var groupWriteAccess = req.body.groupWriteAccess || false;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Sharing synoptic ', id, ' groups ', groups);
  Synoptic.findById(id, function(err, synoptic) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!synoptic) {
      res.sendStatus(404);
      return;
    }
    if (synoptic.user !== username) {
      res.sendStatus(403);
      return;
    }
    synoptic.group = group;
    synoptic.groupWriteAccess = groupWriteAccess;
    synoptic.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log(err);
      } else {
        res.send({ id: synoptic._id + '', created: false });
      }
    });
  });
});
/**
 * Clone a synoptic using id
 */
router.post('/:id/clone', function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Clone synoptic  username: ', username, ' groups ', groups);
  Synoptic.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(404);
    } else {
      if (result) {
        insertSynoptic(
          result.name + ' - copy',
          username,
          result.svg,
          result.variables,
          result.tangoDB,
          res);
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Rename a synoptic using id
 * returns id
 */
router.post('/:id/rename', function(req, res, next) {
  var id = req.params.id;
  var new_name = req.body.newName;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Rename synoptic for username: ', username, ' groups ', groups);
  Synoptic.findOneAndUpdate(
    {
      _id: new mongoose.mongo.ObjectID(id),
      user: username,
      deleted: { $ne: true },
    },
    { $set: { name: new_name } },
    { new: true },
    function(err, result) {
      if (err) {
        res.send(404);
      } else {
        if (result) {
          res.send({ id: result._id });
        } else {
          res.sendStatus(404);
        }
      }
    },
  );
});
/**
 * Creates or updates a synoptic.
 * returns
 * id:string - the id of the synoptic that was updated or created
 * created:boolean - true if the synoptic was created
 */
router.post('/', function(req, res, next) {
  var body = req.body;
  var id = body.id;
  var name = body.name;
  var svg = body.svg;
  var tangoDB = body.tangoDB || '';
  var token = req.cookies.taranta_jwt;
  var variables = body.variables;
  const { username, groups } = decodeToken(token);
  console.log('Variables: ', variables);

  if (id && id !== '') {
    try {
      updateSynoptic(id, username, groups, svg, variables, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  } else {
    try {
      insertSynoptic(name, username, svg, variables, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  }
});

const updateSynoptic = (id, username, groups, svg,
  variables, tangoDB, res) => {
  console.log('Updating synoptic ' + id);
  Synoptic.findById(id, function(err, synoptic) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!synoptic) {
      res.sendStatus(404);
      return;
    }
    const mine = synoptic.user === username;
    const sharedWithMe = synoptic.group &&
        groups.includes(synoptic.group) &&
        synoptic.groupWriteAccess;
    if (!(mine || sharedWithMe)) {
      res.sendStatus(403);
      return;
    }
    synoptic.svg = svg;
    synoptic.variables = variables;
    synoptic.updateTime = new Date();
    synoptic.lastUpdatedBy = username;
    synoptic.tangoDB = tangoDB;
    synoptic.markModified('svg');
    synoptic.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log(err);
      } else {
        res.send({ id: synoptic._id + '', created: false });
      }
    });
  });
};
const insertSynoptic = (name, username, svg, variables, tangoDB, res) => {
  var synoptic = new Synoptic();
  console.log('Inserting synoptic ' + synoptic._id);
  synoptic.svg = svg;
  synoptic.variables = variables;
  synoptic.user = username;
  synoptic.name = name || 'Untitled synoptic';
  synoptic.insertTime = new Date();
  synoptic.lastUpdatedBy = username;
  synoptic.tangoDB = tangoDB;
  synoptic.save(function(err) {
    if (err) {
      res.sendStatus(500);
      console.log('ERROR?');
      console.dir(err);
    } else {
      res.send({ id: synoptic._id + '', created: true });
    }
  });
};

const decodeToken = token => {
  try {
    const decoded = jsonwebtoken.verify(token, process.env.SECRET);
    return decoded;
  } catch (exception) {
    throw Error('jwt authorization failed');
  }
};
exports.decodeToken = decodeToken;
exports.router = router;
